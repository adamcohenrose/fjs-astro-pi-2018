#!/usr/bin/python3

import evdev
from sense_hat import SenseHat
import os
import runpy
from time import sleep
from sense_hat_display_utils.utility import SenseHatUtility, Colour
import logging
import sys


# set up logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

stdoutHandler = logging.StreamHandler(sys.stdout)
stdoutHandler.setLevel(logging.INFO)

logFormatter = logging.Formatter("%(asctime)s | %(levelname)s | %(message)s")
stdoutHandler.setFormatter(logFormatter)

logger.addHandler(stdoutHandler)


# wiring for my astro pi buttons is a bit weird
#     KEY_U
# KEY_L   KEY_D
#   (nothing)
#
# KEY_A   KEY_B

buttonA = evdev.ecodes.KEY_A
buttonB = evdev.ecodes.KEY_B
buttonL = evdev.ecodes.KEY_L
buttonR = evdev.ecodes.KEY_D

# event0 is the gpio buttons (event1 is the joystick)
device = evdev.InputDevice('/dev/input/event0')

sense = SenseHat()
shUtil = SenseHatUtility(autorestore=False)

file = None

O = (0, 0, 0)
g = (0, 200, 0)
r = (200, 0, 0)
button_key = [
  O, O, O, O, O, O, O, O,
  O, O, g, O, O, O, g, O,
  O, O, O, O, O, O, O, O,
  r, O, O, O, r, O, O, O,
  O, O, O, O, O, O, O, O,
  O, O, g, O, O, O, g, O,
  O, O, O, O, O, O, O, O,
  O, O, O, O, O, O, O, O,
]

if __name__ == "__main__":
    try:
        logger.info("starting loop...")
        sense.set_pixels(button_key)

        while True:
            event = device.read_one()
            if event is not None and event.type == evdev.ecodes.EV_KEY and event.value == 1:
                if event.code == buttonL:
                    file = "team_bob"
                elif event.code == buttonR:
                    file = "team_watermeloons"
                elif event.code == buttonA:
                    file = "novalyesha"
                elif event.code == buttonB:
                    file = "uniphoenix"
                else:
                    file = None

            if file != None:
                try:
                    logger.info("running file %s" % file)
                    runpy.run_path(file + ".py")
                    logger.info("finished running file %s" % file)
                    shUtil.fade_out(speed=0.01)
                except KeyboardInterrupt as interrupt:
                    raise interrupt
                except:
                    logger.exception("caught exception in %s" % file)
                    pass
                finally:
                    # flush buffer of button presses
                    while device.read_one() != None:
                        pass
                    file = None
                    sense.set_pixels(button_key)

    # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        logger.info("exiting...")
        pass
    finally:
        sense.clear()
