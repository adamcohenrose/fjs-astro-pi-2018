from sense_hat import SenseHat
from time import sleep
sense = SenseHat()
sense.set_rotation(270)
red = (190, 33, 23)
green = (71, 116, 19)
sense.show_message("TEAM WATERMELOONS!!!", scroll_speed=0.10, back_colour=green, text_colour=red)

w = (255, 255, 255)
b = (0, 0, 0)
r = (190, 33, 23)
g = (71, 116, 19)

picture = [
  w,w,w,w,w,w,w,w,
  w,w,g,w,w,w,w,w,
  w,g,b,r,w,w,w,w,
  w,g,r,r,r,w,w,w,
  w,g,r,r,b,r,w,w,
  w,w,g,r,r,r,r,w,
  w,w,w,g,r,r,b,g,
  w,w,w,w,g,g,g,w
]
sense.set_pixels(picture)
sleep(2)
temp = round( sense.get_temperature(), 1 )
sense.show_message( "IT IS! " + str(temp) + "  DEGREES!" )
