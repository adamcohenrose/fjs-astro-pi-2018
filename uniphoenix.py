from sense_hat import SenseHat
from time import sleep

sense = SenseHat()
sense.set_rotation(270)
message = "Uniphoenix (Annie & Hannah) have a great flight"
green = (0,255,0)
red = (255,0,0)
sense.show_message(message, text_colour=green,back_colour=red, scroll_speed=0.07)


r = (252, 6, 5)
o = (254, 174, 96)
p = (255, 126, 255)
b = (8, 3, 3)
w = (255, 254, 255)
picture = [
b,w,b,b,b,b,w,b,
b,b,w,b,b,w,b,b,
b,b,b,w,w,b,b,b,
r,b,b,p,p,b,b,r,
o,r,b,p,p,b,r,o,
r,o,r,p,p,r,o,r,
b,r,o,r,r,o,r,b,
b,b,b,b,b,b,b,b
]

sense.set_pixels(picture)
sleep(0.7)

temp = round( sense.get_temperature(), 1 )
sense.show_message(str(temp) + " degrees" )
