from sense_hat import SenseHat
sense = SenseHat()
sense.set_rotation(270)
sense.show_message('**Meme Review', text_colour= (11,42,82))
temp=round(sense.get_temperature(),1)
sense.show_message( str(temp))
b=(0,0,0)
w=(255,255,255)
r=(255,0,0)
g=(0,255,0)
p=(0,0,255)
y=(255,255,0)
from time import sleep

picture = [
  b,w,b,w,b,w,b,w,
  w,b,w,b,w,b,w,b,
  b,w,b,w,b,w,b,w,
  w,b,w,b,w,b,w,b,
  b,w,b,w,b,w,b,w,
  w,b,w,b,w,b,w,b,
  b,w,b,w,b,w,b,w,
  w,b,w,b,w,b,w,b
]
sense.set_pixels(picture)

sleep(4)

picture = [
 y,y,y,y,y,y,y,y,
 p,p,p,p,p,p,p,p,
 g,g,g,g,g,g,g,g,
 r,r,r,r,r,r,r,r,
 r,r,r,r,r,r,r,r,
 g,g,g,g,g,g,g,g,
 p,p,p,p,p,p,p,p,
 y,y,y,y,y,y,y,y,
]
sense.set_pixels(picture)

picture = [
 y,p,g,r,r,g,p,y,
 y,p,g,r,r,g,p,y,
 y,p,g,r,r,g,p,y,
 y,p,g,r,r,g,p,y,
 y,p,g,r,r,g,p,y,
 y,p,g,r,r,g,p,y,
 y,p,g,r,r,g,p,y,
 y,p,g,r,r,g,p,y,
 ]
sleep(4)
sense.set_pixels(picture)

picture = [
  p,p,b,y,r,g,r,w,
  p,y,p,g,y,r,g,g,
  p,y,p,g,p,p,y,p,
  y,p,p,g,p,g,p,p,
  r,y,g,g,r,g,p,r,
  y,p,y,p,g,y,p,y,
  r,p,g,g,y,y,r,p,
  y,g,y,p,y,p,r,g,
]
sleep(4)
sense.set_pixels(picture)






